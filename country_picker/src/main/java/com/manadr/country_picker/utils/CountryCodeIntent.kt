package com.manadr.country_picker.utils

import android.content.Intent
import com.manadr.country_picker.ui.CountryCodeChooserActivity
import com.manadr.country_picker.model.CountryCode

object CountryCodeIntent {
    operator fun get(intent: Intent?) =
            intent?.getParcelableExtra<CountryCode?>(CountryCodeChooserActivity.EXTRA_COUNTRY)
}
