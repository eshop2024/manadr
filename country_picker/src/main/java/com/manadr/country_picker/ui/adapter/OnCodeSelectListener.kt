package com.manadr.country_picker.ui.adapter

import androidx.annotation.RestrictTo
import com.manadr.country_picker.model.CountryCode

@RestrictTo(RestrictTo.Scope.LIBRARY)
interface OnCodeSelectListener {
    fun onCodeSelected(countryCode: CountryCode)
}
