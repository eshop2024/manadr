package com.manadr.country_picker.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RestrictTo
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manadr.country_picker.ui.adapter.CountryCodeAdapter
import com.manadr.country_picker.ui.adapter.CountryCodeHeaderAdapter
import com.manadr.country_picker.ui.adapter.OnCodeSelectListener
import com.manadr.country_picker.utils.AssetsReader
import com.manadr.country_picker.R
import com.manadr.country_picker.model.CountryCode
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import java.util.*

private const val CODES_FILE = "country_codes.json"

@RestrictTo(RestrictTo.Scope.LIBRARY)
class CountryCodeChooserFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: CountryCodeHeaderAdapter

    private val countryList: List<CountryCode>
        get() {
            val codes = AssetsReader.read(context, CODES_FILE, Array<CountryCode>::class.java)
            Collections.sort(codes)
            return codes
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_country_code_chooser, container, false)
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(requireActivity(),DividerItemDecoration.VERTICAL ))
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val countryList = countryList
        val baseAdapter = CountryCodeAdapter()
        adapter = CountryCodeHeaderAdapter(baseAdapter)
        baseAdapter.update(countryList)
        adapter.setOnCodeSelectListener(object : OnCodeSelectListener {
            override fun onCodeSelected(countryCode: CountryCode) {
                onCountrySelected(countryCode)
            }

        })
        var position = 0
        arguments?.let {
            val countryCode = it.getParcelable<CountryCode>(CountryCodeChooserActivity.EXTRA_COUNTRY)
            val countryFrequently = it.getParcelableArrayList<CountryCode>(CountryCodeChooserActivity.EXTRA_FREQUENTLY_COUNTRY)
            countryCode?.let {
                position = countryList.indexOf(it)
                adapter.setSelected(it)
            }
        }
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(StickyRecyclerHeadersDecoration(adapter))
        if (position > 0) {
            recyclerView.scrollToPosition(position - 1)
        }
    }

    private fun onCountrySelected(countryCode: CountryCode) {
        val activity = activity
        (activity as? CountryCodeChooserActivity)?.onCountrySelected(countryCode)
    }

    fun onSearchChange(query: String) {
        adapter.setFilter(query)
        recyclerView.scrollToPosition(0)
    }
}
