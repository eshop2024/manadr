package com.manadr.country_picker.ui.adapter

import androidx.annotation.RestrictTo
import androidx.recyclerview.widget.RecyclerView
import com.manadr.country_picker.model.CountryCode
import com.manadr.country_picker.ui.adapter.sections.SectionCountry
import com.manadr.country_picker.ui.adapter.sections.SectionDataWrapper

@RestrictTo(RestrictTo.Scope.LIBRARY)
abstract class CountryCodeSectionAdapter<T : RecyclerView.ViewHolder>
internal constructor(protected val adapter: CountryCodeAdapter)
    : RecyclerView.Adapter<T>(), HeaderAdapter {
    private val sectionDataWrapper = SectionDataWrapper()

    init {
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                val codes = adapter.data
                sectionDataWrapper.onChanged(codes)
            }
        })
    }

    override fun setOnCodeSelectListener(onCodeSelectListener: OnCodeSelectListener) {
        adapter.setOnCodeSelectListener(onCodeSelectListener)
    }

    override fun setSelected(selected: CountryCode) {
        adapter.setSelected(selected)
    }

    override fun setFilter(filter: String) {
        sectionDataWrapper.setFilter(filter)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return sectionDataWrapper.itemCount
    }

    internal fun getSectionCountry(position: Int): SectionCountry {
        return sectionDataWrapper.getSectionCountry(position)
    }
}
