package com.manadr.country_picker.ui.adapter

import androidx.annotation.RestrictTo
import com.manadr.country_picker.model.CountryCode

@RestrictTo(RestrictTo.Scope.LIBRARY)
interface HeaderAdapter {

    fun setOnCodeSelectListener(onCodeSelectListener: OnCodeSelectListener)

    fun setSelected(selected: CountryCode)

    fun setFilter(filter: String)

    companion object {
        val HEADERS = arrayOf("OTHERS")
    }
}
