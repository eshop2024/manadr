package com.manadr.country_picker.ui

interface OnSearchInputListener {
    fun onSearch(query: String)
}
