package com.manadr.country_picker.ui.adapter.sections

import androidx.annotation.RestrictTo
import com.manadr.country_picker.model.CountryCode

@RestrictTo(RestrictTo.Scope.LIBRARY)
data class SectionCountry internal constructor(val section: Section, val code: CountryCode)
