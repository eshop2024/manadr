package com.manadr.country_picker.ui.adapter.sections

import androidx.annotation.RestrictTo

@RestrictTo(RestrictTo.Scope.LIBRARY)
data class Section internal constructor(val id: String) {
    companion object {
        val EMPTY_SECTION = Section("OTHERS")
    }
}
