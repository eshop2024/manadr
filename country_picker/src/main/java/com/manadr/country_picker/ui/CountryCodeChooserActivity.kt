package com.manadr.country_picker.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.manadr.country_picker.R
import com.manadr.country_picker.databinding.ActivityCountryCodeChooserBinding
import com.manadr.country_picker.model.CountryCode

class CountryCodeChooserActivity : AppCompatActivity(),
    SearchView.OnQueryTextListener,
    SearchView.OnCloseListener {
    private lateinit var fragment: CountryCodeChooserFragment
    private lateinit var binding : ActivityCountryCodeChooserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.white)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_country_code_chooser)

        if (savedInstanceState == null) {
            fragment = CountryCodeChooserFragment()
            fragment.arguments = intent.extras
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, TAG_FRAGMENT)
                .commitAllowingStateLoss()
        } else {
            fragment = supportFragmentManager.findFragmentByTag(TAG_FRAGMENT) as CountryCodeChooserFragment
        }

        binding.imgBack.setOnClickListener { finish() }
        binding.searchView.setOnQueryTextListener(this)
        binding.searchView.setOnCloseListener(this)
    }

    fun onCountrySelected(countryCode: CountryCode) {
        setResult(Activity.RESULT_OK, Intent().putExtra(EXTRA_COUNTRY, countryCode))
        finish()
    }

    private fun onSearch(query: String) {
        fragment.onSearchChange(query)
    }

    companion object {
        const val EXTRA_COUNTRY = "key_country"
        const val EXTRA_FREQUENTLY_COUNTRY = "key_frequently_country"
        private const val TAG_FRAGMENT = "CountryCodeChooserFragment"

        fun start(activity: Activity, countryCode: CountryCode?, frequentlyCountry: ArrayList<CountryCode>, requestCode: Int) {
            activity.startActivityForResult(intent(activity, countryCode,frequentlyCountry), requestCode)
        }

//        fun start(fragment: Fragment, countryCode: CountryCode?, requestCode: Int) {
//            fragment.startActivityForResult(
//                intent(fragment.requireActivity(), countryCode),
//                requestCode
//            )
//        }

        private fun intent(
            context: Context,
            countryCode: CountryCode?,
            frequentlyCountry: ArrayList<CountryCode>
        ) =
            Intent(context, CountryCodeChooserActivity::class.java)
                .putExtra(EXTRA_COUNTRY, countryCode)
                .putExtra(EXTRA_FREQUENTLY_COUNTRY, frequentlyCountry)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let {
            onSearch(query)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let {
            onSearch(newText)
        }
        return true
    }

    override fun onClose(): Boolean {
        onSearch("")
        return true
    }
}
