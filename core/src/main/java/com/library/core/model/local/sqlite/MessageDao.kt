package com.library.core.model.local.sqlite

import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.Query

interface MessageDao {

    @Query("SELECT * FROM `message`")
    fun getAllMessage(): LiveData<List<Message>>

    @Insert
    fun insertAllMessage(vararg message: Message)
}