package com.library.core.model.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

abstract class BaseDataSource {

//    protected suspend fun <T> getResult(call: suspend () -> Response<T>): ResponseResult<T> {
//        try {
//            val response = call()
//            if (response.checkSuccess() ) {
//                val body = response.body()
//                if (null != body) return ResponseResult.Success(body)
//            }
//            return error("${response.code()} ${response.message()}")
//        } catch (e: Exception) {
//            return error(e.message ?: e.toString())
//        }
//    }
//
//    private fun <T> error(msg: String): ResponseResult<T> {
//        return ResponseResult.Error(msg)
//    }

}

fun <T> resultLiveData(call: suspend () -> ResponseResult<T>): LiveData<ResponseResult<T>> {
    return liveData{
        emit(ResponseResult.Loading)

        withContext(Dispatchers.IO) {
            emit(call.invoke())
        }
    }
}