package com.library.core.model.network.response

data class AccountResponse(
    val account_id: Int,
    val account_type: String,
    val avatar_url: String,
    val email: String,
    val firstname: String,
    val fullname: String,
    val gender: String,
    val id: Int,
    val is_registered: Boolean,
    val lastname: String,
    val national_id_number: String,
    val phone_country_code: String,
    val phone_number: String,
    val refresh_token: String,
    val token: String,
    val user_id: Int
)