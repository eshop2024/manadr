package com.library.core.model.local.preference

import android.content.SharedPreferences
import javax.inject.Inject

class PreferenceRepository
@Inject constructor(private val preference: SharedPreferences) :
    PreferenceHelper {

    override var token by preference.String("TOKEN", "")

}