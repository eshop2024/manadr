package com.library.core.model.network.response.profile

data class Meta(
    val writable_fields: List<String>
)