package com.library.core.model.network.response

data class VerifyResponse(
    val message: String,
    val status: Boolean
)