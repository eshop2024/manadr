package com.library.core.model.network.response.profile

data class ProfileResponse(
    val accepted_policy: Boolean,
    val account_id: Int,
    val address: String,
    val address_block: Any,
    val address_zip: Any,
    val apartment_number: Any,
    val avatar_url: Any,
    val date_of_birth: Any,
    val drug_allergy: Any,
    val email: String?,
    val firstname: String,
    val gender: String,
    val id: Int,
    val id_medicare: Any,
    val id_number: String,
    val id_worker: Any,
    val is_imported_record: Boolean,
    val issue_country: ResidentCountry?,
    val lastname: String,
    val medical_condition: Any,
    val meta: Meta,
    val phone_country_code: String,
    val phone_number: String,
    val refresh_token: String,
    val register_source: Int,
    val resident_country: ResidentCountry?,
    val token: String,
    val user_id: Int,
    val working_companies: List<WorkingCompany>

)