package com.library.core.model.network.response.profile

data class ResidentCountry(
    val country_code: String,
    val currency_code: String,
    val first_id_letters: List<String>,
    val id: Int,
    val is_frequently_selected: Boolean,
    val iso: String,
    val iso3: String,
    val name: String,
    val nice_name: String,
    val phone_country_code: String,
    val region: String,
    val sub_currency_amount: Int,
    val sub_region: String
)