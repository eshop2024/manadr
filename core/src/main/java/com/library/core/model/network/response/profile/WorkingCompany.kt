package com.library.core.model.network.response.profile

data class WorkingCompany(
    val address: String,
    val charge_gst: Boolean,
    val code: Any,
    val created_at: String,
    val display_at_home: Boolean,
    val email: String,
    val id: Int,
    val img_url: String,
    val is_featured_partner: Boolean,
    val is_manadr_deal: Boolean,
    val is_partner: Boolean,
    val is_special_chat_fee: Boolean,
    val is_special_video_fee: Boolean,
    val is_staff: Boolean,
    val name: String,
    val phone: String,
    val promotions: Any,
    val tax_profile: Any,
    val tax_profile_id: Any,
    val updated_at: String
)