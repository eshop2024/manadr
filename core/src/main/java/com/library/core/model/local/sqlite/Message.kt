package com.library.core.model.local.sqlite

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message")
class Message {
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id = 0

    @ColumnInfo(name = "from")
    var from = ""

    @ColumnInfo(name = "from")
    var message = ""

    @ColumnInfo(name = "type")
    var type = 1
}