package com.library.core.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.support.DaggerFragment
import com.library.core.di.viewmodel.ViewModelProviderFactory
import javax.inject.Inject

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : DaggerFragment() {

    private var baseActivity: BaseActivity<*,*>? = null

    private var mRootView: View? = null

    protected lateinit var binding: T

    protected lateinit var viewModel: V

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun bindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun layoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun viewModel(): V

    abstract fun initComponent()

    abstract fun subscribeData()

    @set:Inject
    lateinit var factory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        //performDependencyInjection()
        super.onCreate(savedInstanceState)
        this.viewModel = viewModel()
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        mRootView = binding.root
        return mRootView
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(bindingVariable(), viewModel)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        initComponent()
        subscribeData()
    }

//    fun hideKeyboard() {
//        if (baseActivity != null) {
//            baseActivity!!.hideKeyboard()
//        }
//    }

//    fun openActivityOnTokenExpire() {
//        if (baseActivity != null) {
//            baseActivity!!.openActivityOnTokenExpire()
//        }
//    }

//    private fun performDependencyInjection() {
//        AndroidSupportInjection.inject(this)
//    }

    fun isInitialized(): Boolean = this::binding.isInitialized
}