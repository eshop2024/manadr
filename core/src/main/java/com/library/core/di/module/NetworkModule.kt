package com.library.core.di.module
import android.content.Context
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.library.core.di.qualifier.BaseUrl
import com.library.core.model.local.preference.PreferenceHelper
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {
    
    private val TIME_OUT = 60

    @Singleton
    @Provides
    fun providesClient(
        preferenceHelper: PreferenceHelper,
        context: Context
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .readTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .callTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor( ChuckInterceptor(context))
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC).setLevel(
                    HttpLoggingInterceptor.Level.BODY
                ).setLevel(HttpLoggingInterceptor.Level.HEADERS)
            )
            .addInterceptor { chain: Interceptor.Chain ->
                chain.proceed(chain.request().newBuilder()
                        .addHeader("Authorization", preferenceHelper.token)
                        .build()
                )
            }
        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideChuckInterceptor(context: Context): ChuckInterceptor = ChuckInterceptor(context)

    @Singleton
    @Provides
    fun provideRetrofit(@BaseUrl baseUrl : String,context: Context, preferenceHelper: PreferenceHelper): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(providesClient(preferenceHelper,context))
            .build()
    }

}
