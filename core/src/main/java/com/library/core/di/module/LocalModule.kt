package com.library.core.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.library.core.di.qualifier.PreferenceInfo
import dagger.Module
import dagger.Provides
import com.library.core.model.local.preference.PreferenceHelper
import com.library.core.model.local.preference.PreferenceRepository
import javax.inject.Singleton

@Module
class LocalModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun providePreference(context: Context, @PreferenceInfo name : String): SharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    internal fun providePrefRepository(preferenceRepository: PreferenceRepository): PreferenceHelper = preferenceRepository

}
