package com.example.manadr.di.components

import android.app.Application
import com.example.manadr.ManaDrMainApplication
import com.library.core.di.module.LocalModule
import com.library.core.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import com.example.manadr.di.builders.ActivityBuilder
import com.example.manadr.di.modules.ManaDrAppModule
import com.example.manadr.di.modules.RepositoryModule
import com.example.manadr.di.viewmodel.ViewModelFactoryModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class,
        NetworkModule::class,
        LocalModule::class,
        ManaDrAppModule::class,
        ActivityBuilder::class,
        ViewModelFactoryModule::class,
        RepositoryModule::class])

interface ManaDrComponent : AndroidInjector<ManaDrMainApplication> {

    override fun inject(app: ManaDrMainApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ManaDrComponent

    }
}