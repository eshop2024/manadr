package com.example.manadr.di.builders.providers

import com.example.manadr.features.on_boarding.detail.OnboardingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class OnBoardingFragmentProvider {

    @ContributesAndroidInjector()
    abstract fun provideOnboardingFragment(): OnboardingFragment

}
