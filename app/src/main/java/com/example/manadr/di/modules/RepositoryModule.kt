package com.example.manadr.di.modules

import com.example.manadr.features.profile.info.ProfileRepository
import com.example.manadr.features.profile.info.ProfileRepositoryImp
import com.example.manadr.features.authen.verify_phone_number.LoginRepository
import com.example.manadr.features.authen.verify_phone_number.LoginRepositoryImp
import com.example.manadr.features.profile.register.ProfileRegisterRepository
import com.example.manadr.features.profile.register.ProfileRegisterRepositoryImp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    internal fun provideLoginRepository(imp: LoginRepositoryImp): LoginRepository = imp

    @Provides
    @Singleton
    internal fun provideProfileRegisterRepository(imp: ProfileRegisterRepositoryImp): ProfileRegisterRepository = imp

    @Provides
    @Singleton
    internal fun provideProfileRepository(imp: ProfileRepositoryImp): ProfileRepository = imp
}