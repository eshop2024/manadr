package com.example.manadr.di.builders.providers

import com.example.manadr.features.main.account.AccountFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AccountFragmentProvider {

    @ContributesAndroidInjector()
    abstract fun provideAccountFragment(): AccountFragment

}
