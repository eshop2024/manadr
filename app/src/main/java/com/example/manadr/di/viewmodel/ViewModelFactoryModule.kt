package com.example.manadr.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.manadr.features.profile.info.ProfileViewModel
import com.example.manadr.features.authen.otp_screen.OTPVerifyViewModel
import com.example.manadr.features.authen.verify_phone_number.VerifyPhoneViewModel
import com.example.manadr.features.main.MainViewModel
import com.example.manadr.features.on_boarding.OnBoardingVieModel
import com.example.manadr.features.profile.register.ProfileRegisterViewModel
import com.library.core.di.viewmodel.ViewModelKey
import com.library.core.di.viewmodel.ViewModelProviderFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Module to provide [ViewModelProvider.Factory]
 */
@Module
interface ViewModelFactoryModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(OnBoardingVieModel::class)
    fun bindsOnBoardingVieModel(onBoardingVieModel: OnBoardingVieModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyPhoneViewModel::class)
    fun bindsLoginViewModel(loginViewModel: VerifyPhoneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OTPVerifyViewModel::class)
    fun bindsotpVerifyViewModel(otpVerifyViewModel: OTPVerifyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileRegisterViewModel::class)
    fun bindsProfileRegisterViewModel(profileRegisterViewModel: ProfileRegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    fun bindsProfileViewModel(profileRegisterViewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindsMainVieModel(mainVieModel: MainViewModel): ViewModel
}
