package com.example.manadr.di.modules

import com.library.core.di.qualifier.BaseUrl
import com.library.core.di.qualifier.PreferenceInfo
import android.content.Context
import com.example.manadr.ManaDrMainApplication
import com.library.core.di.qualifier.ApplicationContext
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.example.manadr.models.network.ApiService
import com.example.manadr.models.local.AppPreferenceHelper
import com.example.manadr.models.local.AppPreferenceHelperImp
import com.example.manadr.utils.ApiConstants
import com.example.manadr.utils.Constants
import javax.inject.Singleton

@Module
class ManaDrAppModule {

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String = Constants.PREFERENCES_FILE_NAME

    @Provides
    @BaseUrl
    internal fun provideHost(): String = ApiConstants.BASE_URL

    @Provides
    @Singleton
    internal fun providePostApi(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    internal fun provideAppPreference(preferenceRepository: AppPreferenceHelperImp): AppPreferenceHelper = preferenceRepository

    @Provides
    @ApplicationContext
    internal fun provideContext(application: ManaDrMainApplication): Context {
        return application
    }
}
