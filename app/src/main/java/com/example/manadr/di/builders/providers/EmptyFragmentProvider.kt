package com.example.manadr.di.builders.providers

import com.example.manadr.features.main.empty.EmptyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class EmptyFragmentProvider {

    @ContributesAndroidInjector()
    abstract fun provideEmptyFragment(): EmptyFragment

}
