package com.example.manadr.di.builders

import com.example.manadr.di.builders.providers.AccountFragmentProvider
import com.example.manadr.di.builders.providers.EmptyFragmentProvider
import com.example.manadr.di.builders.providers.OnBoardingFragmentProvider
import com.example.manadr.features.authen.otp_screen.OTPVerificationActivity
import com.example.manadr.features.authen.select_country.SelectCountryCodeActivity
import com.example.manadr.features.authen.verify_phone_number.VerifyPhoneNumberActivity
import com.example.manadr.features.main.MainActivity
import com.example.manadr.features.on_boarding.OnBoardingActivity
import com.example.manadr.features.profile.info.ProfileActivity
import com.example.manadr.features.profile.register.RegisterProfileActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [OnBoardingFragmentProvider::class])
    abstract fun bindOnBoardingActivity(): OnBoardingActivity

    @ContributesAndroidInjector()
    abstract fun bindLoginActivity(): VerifyPhoneNumberActivity

    @ContributesAndroidInjector()
    abstract fun bindSelectCountryCodeActivity(): SelectCountryCodeActivity

    @ContributesAndroidInjector()
    abstract fun bindOTPVerificationActivity(): OTPVerificationActivity

    @ContributesAndroidInjector()
    abstract fun bindRegisterProfileActivity(): RegisterProfileActivity

    @ContributesAndroidInjector()
    abstract fun bindProfileActivity(): ProfileActivity

    @ContributesAndroidInjector(modules = [EmptyFragmentProvider::class, AccountFragmentProvider::class])
    abstract fun bindMainActivity(): MainActivity
}
