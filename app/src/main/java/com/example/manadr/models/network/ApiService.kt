package com.example.manadr.models.network

import com.example.manadr.models.network.entity.ProfileRequest
import com.library.core.model.network.response.AccountResponse
import com.library.core.model.network.response.profile.ProfileResponse
import retrofit2.Response
import retrofit2.http.*
import com.library.core.model.network.response.VerifyResponse
import com.example.manadr.utils.ApiConstants

interface ApiService {
    @FormUrlEncoded
    @Headers("X-Api-Key: " + ApiConstants.SECURITY_KEY)
    @POST(ApiConstants.REQUEST_PHONE_VERIFICATION)
    suspend fun requestPhoneVerification(@Field("phone_country_code") countryCode: String,
                                         @Field("phone_number") phoneNumber: String,
                                         @Field("receive_via") receiveVia: String): Response<VerifyResponse>

    @FormUrlEncoded
    @Headers("X-Api-Key: " + ApiConstants.SECURITY_KEY)
    @POST(ApiConstants.PHONE_VERIFICATION)
    suspend fun phoneVerification(@Field("phone_country_code") countryCode: String,
                                  @Field("phone_number") phoneNumber: String,
                                  @Field("otp") otp: String): Response<AccountResponse>

    @Headers("X-Api-Key: " + ApiConstants.SECURITY_KEY)
    @POST(ApiConstants.REGISTER_PROFILE)
    suspend fun registerProfile(@Body profile : ProfileRequest): Response<ProfileResponse>

    @Headers("X-Api-Key: " + ApiConstants.SECURITY_KEY)
    @GET(ApiConstants.GET_PROFILE)
    suspend fun getProfile(): Response<ProfileResponse>
}