package com.example.manadr.di.model.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Boarding(
    val title: String? = null,
    val content: String? = null,
    val image: Int
) : Parcelable