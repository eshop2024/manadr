package com.example.manadr.models.local

import android.content.SharedPreferences
import com.manadr.country_picker.model.CountryCode
import com.library.core.model.local.preference.Arrays
import com.library.core.model.local.preference.Object
import com.example.manadr.utils.Constants
import javax.inject.Inject

class AppPreferenceHelperImp @Inject constructor(private val preference: SharedPreferences) :
    AppPreferenceHelper {
    override var country: CountryCode? by preference.Object(Constants.COUNTRY, null)
    override var frequentlyCountry: ArrayList<CountryCode> by preference.Arrays(Constants.FREQUENTLY, arrayListOf())
}