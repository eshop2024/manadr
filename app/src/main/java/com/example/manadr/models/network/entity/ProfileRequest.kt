package com.example.manadr.models.network.entity

data class ProfileRequest(
    val email: String,
    val firstname: String,
    val id_number: String,
    val lastname: String
)