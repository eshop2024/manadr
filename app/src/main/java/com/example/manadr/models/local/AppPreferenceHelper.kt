package com.example.manadr.models.local

import com.manadr.country_picker.model.CountryCode

interface AppPreferenceHelper {
    var country: CountryCode?

    var frequentlyCountry: ArrayList<CountryCode>
}