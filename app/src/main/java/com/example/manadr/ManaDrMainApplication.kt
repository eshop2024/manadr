package com.example.manadr

import com.example.manadr.di.components.DaggerManaDrComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class ManaDrMainApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerManaDrComponent.builder().application(this).build()
    }
}