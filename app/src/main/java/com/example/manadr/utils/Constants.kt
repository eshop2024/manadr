package com.example.manadr.utils

interface Constants {
    companion object {
        const val PHONE_BUNDLE = "phone"
        const val COUNTRY_CODE_BUNDLE = "country"
        const val VERIFICATION_TYPE_BUNDLE = "verification type"
        const val ANIMATION_SLIDE_RIGHT_TO_LEFT = 1
        const val DEFAULT_INTERVAL = 300
        const val ANIMATION_SLIDE_LEF_TO_RIGHT = 2
        const val PREFERENCES_FILE_NAME = "MANADR_PREFERENCES"
        const val COUNTRY = "country"
        const val FREQUENTLY = "frequently country"
        const val REQUEST_COUNTRY_CODE = 100
    }
}