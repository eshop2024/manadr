package com.example.manadr.utils.extension

import android.util.Patterns
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.zip
import java.text.Normalizer


inline fun <reified T> fromJson(json: String): T {
    return Gson().fromJson(json, object: TypeToken<T>(){}.type)
}

fun <T1, T2, T3, R> zip3(
    first: Flow<T1>,
    second: Flow<T2>,
    third: Flow<T3>,
    transform: suspend (T1, T2, T3) -> R
): Flow<R> =
    first.zip(second) { a, b -> a to b }
        .zip(third) { (a, b), c ->
            transform(a, b, c)
        }

// xóa dấu tiếng việt
fun String.unaccent(): String {
    val regexA = Regex("[^\\p{ASCII}]")
    return Normalizer
        .normalize(this, Normalizer.Form.NFD)
        .replace(regexA, "")
}

fun String.deleteAccentsDeleteSpacesLowercaseLetters():String {
    val c = this.unaccent()
    val a = c.replace(" ", "")
    val b = a.toLowerCase()
    return b
}

fun String.isPhoneNumber() : Boolean{
    val actualPhone = if (this.subSequence(0,1) == "0") this.substring(1) else this
    val valid =  Patterns.PHONE.matcher(actualPhone).matches()
    return valid && actualPhone.length == 9
}

fun String.toPhoneFormat(): String? {
    return if (this.subSequence(0,1) == "0") this.substring(1) else this
}

fun String.isEmail() : Boolean{
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}