package com.example.manadr.utils

data class PromotionStatusColor(
    val statusColor: StatusColor,
    val data: String
)

enum class StatusColor {
    NORMAL,
    RED,
    GREEN
}

enum class Language {
    US,
    VN
}

enum class CouponUIEvent {
    USE_NOW,
    USE_LATER
}

enum class EventVerifyType {
    SHOW_ERROR_DIALOG, PROCEED_CHECKOUT, DEFAULT
}