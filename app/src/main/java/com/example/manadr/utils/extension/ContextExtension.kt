package com.example.manadr.utils.extension

import android.Manifest
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import org.joda.time.format.DateTimeFormat
import com.library.core.R
import com.example.manadr.utils.Constants.Companion.ANIMATION_SLIDE_LEF_TO_RIGHT
import com.example.manadr.utils.Constants.Companion.ANIMATION_SLIDE_RIGHT_TO_LEFT
import java.text.SimpleDateFormat
import java.util.*

fun Activity.changeStatusBarColor(color: Int, lighStatus: Boolean? = false) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val startColor = this.window.statusBarColor
        val endColor = ContextCompat.getColor(this, color)
        ObjectAnimator.ofArgb(this.window, "statusBarColor", startColor, endColor).start()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && lighStatus!!) {
            this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }
}

inline fun <reified T : Activity> Context.openActivity(extras: Bundle.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.putExtras(Bundle().apply(extras))
    startActivity(intent)
}

inline fun <reified T : Any> Activity.extra(key: String, default: T? = null) = lazy {
    val value = intent?.extras?.get(key)
    if (value is T) value else default
}

fun Context.checkPermissions(
    vararg permissions: String,
    onGranted: () -> Unit,
    onDeny: () -> Unit
) {
    val neededPermissionsCheck = permissions.filter {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_DENIED
    }.toTypedArray()

    if (neededPermissionsCheck.isNotEmpty()) {
        val listener = object : PermissionListener {
            override fun onPermissionGranted() {
                onGranted.invoke()
            }

            override fun onPermissionDenied(deniedPermissions: List<String>) {
                onDeny.invoke()
            }
        }

        TedPermission.with(this)
            .setPermissionListener(listener)
            .setPermissions(*neededPermissionsCheck)
            .check()
    } else {
        onGranted.invoke()
    }
}

fun Context.getWidthScreen(): Int {
    val displayMetrics = DisplayMetrics()
    (this as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun Context.getHeightScreen(): Int {
    val displayMetrics = DisplayMetrics()
    (this as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun Context.hasInternetConnection(): Boolean {
    var result = false
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.run {
                result = when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }

            }
        }
    }

    return result
}

fun String.getFormatTimeServer(): Date {
    val normal = "yyyy-MM-dd'T'HH:mm:ss"
    val UTC = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
    val specialUTC = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    var dateFormat = SimpleDateFormat(normal)
    if (this.contains("Z"))
        dateFormat = SimpleDateFormat(UTC)
    try {
        return dateFormat.parse(this)
    } catch (e: Exception) {
        return SimpleDateFormat(specialUTC).parse(this)
    }
}

fun Context.convertDpToPx(dp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )
}

fun Context.convertPxToDp(px: Float): Float {
    return px / resources.displayMetrics.density
}


fun Activity.detectKeyboardShow(
    onShow: ((height: Int) -> Unit)? = null,
    onClose: (() -> Unit)? = null
) {
// ContentView is the root view of the layout of this activity/fragment
    var isKeyboardShowing = false
    val contentView = findViewById<View>(android.R.id.content)
    contentView.viewTreeObserver.addOnGlobalLayoutListener {
        val rect = Rect()
        contentView.getWindowVisibleDisplayFrame(rect)
        val screenHeight = contentView.rootView.height
        val keypadHeight: Int = screenHeight - rect.bottom
        if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
            // keyboard is opened
            if (!isKeyboardShowing) {
                isKeyboardShowing = true
                onShow?.invoke(keypadHeight)
                //contentView.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        } else {
            // keyboard is closed
            if (isKeyboardShowing) {
                isKeyboardShowing = false

            }
        }
    }
}

fun Activity.is189ScreenAbove(): Boolean {
    val activity = this
    val metrics = DisplayMetrics().apply {
        activity.windowManager.defaultDisplay.getMetrics(this)
    }
    val ratio = metrics.heightPixels.toFloat() / metrics.widthPixels.toFloat()

    return ratio > (17 / 9)
}

interface RequestPermissionListener {
    /**
     * Callback on permission previously denied
     * should show permission rationale and continue permission request
     */
    fun onPermissionRationaleShouldBeShown(requestPermission: () -> Unit)

    /**
     * Callback on permission "Never show again" checked and denied
     * should show message and open app setting
     */
    fun onPermissionPermanentlyDenied(openAppSetting: () -> Unit)

    /**
     * Callback on permission granted
     */
    fun onPermissionGranted()
}

fun shouldRequestRuntimePermission(): Boolean {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
}

fun Context.shouldAskPermissions(permissions: String): Boolean {
    if (shouldRequestRuntimePermission()) {

        if (ContextCompat.checkSelfPermission(this, permissions)
            != PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }

    }
    return false
}

@RequiresApi(Build.VERSION_CODES.M)
fun shouldShowRequestPermissionsRationale(context: Context, permissions: String): Boolean {
    if ((context as Activity).shouldShowRequestPermissionRationale(permissions)) {
        return true
    }
//    var a = ContextCompat.checkSelfPermission(context, permissions)
//    if (ContextCompat.checkSelfPermission(context, permissions) == -1){
//        return true
//    }
    return false
}

fun Context.isFirstTimeAskingPermissions(permissions: String): Boolean {
    val sharedPreference: SharedPreferences? = getSharedPreferences(packageName, MODE_PRIVATE)
    if (sharedPreference?.getBoolean(permissions, true) == true) {
        return true
    }
    return false
}

fun Context.firstTimeAskingPermissions(permissions: String, isFirstTime: Boolean) {
    val sharedPreference: SharedPreferences? = getSharedPreferences(packageName, MODE_PRIVATE)
    sharedPreference?.edit()?.putBoolean(permissions, isFirstTime)?.apply()
}

fun Context.openAppDetailSettings() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    }
}

@RequiresApi(Build.VERSION_CODES.M)
fun Context.requestPermissions2(
    context: Context,
    permissions: String,
    permissionRequestCode: Int,
    requestPermissionListener: RequestPermissionListener
) {
    // permissions is not granted
    if (shouldAskPermissions(permissions)) {
        // permissions denied previously
        var a = shouldShowRequestPermissionsRationale(context, permissions)
        if (a) {
            requestPermissionListener.onPermissionRationaleShouldBeShown {
//                requestPermissions2(
//                    context,
//                    permissions,
//                    permissionRequestCode,
//                    requestPermissionListener
//                )
            }
        } else {
            // Permission denied or first time requested
            var b = context.isFirstTimeAskingPermissions(permissions)
            if (b) {
                context.firstTimeAskingPermissions(permissions, false)
                // request permissions
                ActivityCompat.requestPermissions(
                        context as Activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                99
                )
//                requestPermissions2(
//                    context,
//                    permissions,
//                    permissionRequestCode,
//                    requestPermissionListener
//                )

            } else {
                // permission disabled
                // Handle the feature without permission or ask user to manually allow permission
                requestPermissionListener.onPermissionPermanentlyDenied {
                }
            }
        }
    } else {
        // permission granted
        requestPermissionListener.onPermissionGranted()
    }
}