/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.manadr.utils.binding_adapter

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import org.joda.time.format.DateTimeFormat
import com.library.core.R
import com.library.core.model.network.response.profile.ResidentCountry
import java.io.File
import kotlin.math.roundToInt


/**
 * A collection of [BindingAdapter]s for different UI-related tasks.
 *
 * In Kotlin you can write the Binding Adapters in the traditional way:
 *
 * ```
 * @BindingAdapter("property")
 * @JvmStatic fun propertyMethod(view: ViewClass, parameter1: Param1, parameter2: Param2...)
 * ```
 *
 * Or using extension functions:
 *
 * ```
 * @BindingAdapter("property")
 * @JvmStatic fun ViewClass.propertyMethod(parameter1: Param1, parameter2: Param2...)
 * ```
 *
 * See [EditText.clearTextOnFocus].
 *
 * Also, keep in mind that @JvmStatic is only necessary if you define the methods inside a class or
 * object. Consider moving the Binding Adapters to the top level of the file.
 */
@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    Glide.with(imageView.context).load(url)
        .fitCenter()
        .centerCrop()
        .placeholder(R.drawable.ic_loading)
        .error(R.drawable.ic_loading)
        .into(imageView)
}

@BindingAdapter("imageSrc")
fun setImageSrc(imageView: ImageView, url: Int?) {
    url?.let { imageView.setImageResource(url) }
}


@BindingAdapter("imageFile")
fun setImageFile(imageView: ImageView, url: String?) {
    url?.let {
        Glide.with(imageView.context).load(File(url)).listener(object : RequestListener<Drawable>{
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                val a = 1
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

        }).into(imageView)
    }
}

@BindingAdapter("selected")
fun setSelected(imageView: ImageView, selected: Boolean) {
    imageView.isSelected = selected
}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(current: Boolean?) {
    if (current == null){
        visibility = View.GONE
    }else
        visibility = if (current) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleOrInvisible")
fun View.setVisibleOrInvisible(current: Any?) {
    visibility = if (current == null) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("GoneOrVisible")
fun View.setGoneOrVisible(current: Any?) {
    visibility = if (current != null) View.VISIBLE else View.GONE
}

@BindingAdapter("string_visible")
fun View.setStringVisible(current: String?) {
    current?.let {
        visibility = if (it.trim().isNotEmpty()) View.VISIBLE else View.GONE
    }

}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(checked: Boolean) {
    visibility = if (checked) View.VISIBLE else View.GONE
}

@BindingAdapter("national")
fun TextView.setNational(nation : ResidentCountry?) {
    nation?.let {
        val resources = context.resources
        val resourceId: Int = resources.getIdentifier("country_flag_${nation.iso.toLowerCase()}", "drawable", context.packageName)
        val img = ContextCompat.getDrawable(context, resourceId)

        val density = resources.displayMetrics.density
        val width = (28 * density).roundToInt()
        val height = (18  * density).roundToInt()
        img?.setBounds(0, 0, width, height)
        text = nation.nice_name
        setCompoundDrawables(img, null, null, null)
    }
}

@BindingAdapter("phone_number")
fun TextView.setPhoneNumber(phone : String?) {
    phone?.let {
        val realphone = if (phone.substring(0,1) != "0") "0$phone" else phone
        text = realphone
    }
}

@BindingAdapter("birthday")
fun TextView.setBirthday(birthday : String?) {
    birthday?.let {
        val formatter = DateTimeFormat.forPattern("YYYY-MM")
    }
}