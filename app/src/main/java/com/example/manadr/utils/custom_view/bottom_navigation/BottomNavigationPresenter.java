package com.example.manadr.utils.custom_view.bottom_navigation;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.view.menu.SubMenuBuilder;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.badge.BadgeUtils;
import com.google.android.material.internal.ParcelableSparseArray;

/** @hide */

@SuppressLint("RestrictedApi")
public class BottomNavigationPresenter implements MenuPresenter {
    private MenuBuilder menu;
    private BottomNavigationMenuView menuView;
    private boolean updateSuspended = false;
    private int id;

    public void setBottomNavigationMenuView(BottomNavigationMenuView menuView) {
        this.menuView = menuView;
    }

    @Override
    public void initForMenu(Context context, MenuBuilder menu) {
        this.menu = menu;
        menuView.initialize(this.menu);
    }

    @Override
    public MenuView getMenuView(ViewGroup root) {
        return menuView;
    }

    @Override
    public void updateMenuView(boolean cleared) {
        if (updateSuspended) {
            return;
        }
        if (cleared) {
            menuView.buildMenuView();
        } else {
            menuView.updateMenuView();
        }
    }

    @Override
    public void setCallback(Callback cb) {}

    @Override
    public boolean onSubMenuSelected(SubMenuBuilder subMenu) {
        return false;
    }

    @Override
    public void onCloseMenu(MenuBuilder menu, boolean allMenusAreClosing) {}

    @Override
    public boolean flagActionItems() {
        return false;
    }

    @Override
    public boolean expandItemActionView(MenuBuilder menu, MenuItemImpl item) {
        return false;
    }

    @Override
    public boolean collapseItemActionView(MenuBuilder menu, MenuItemImpl item) {
        return false;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @NonNull
    @Override
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new BottomNavigationPresenter.SavedState();
        savedState.selectedItemId = menuView.getSelectedItemId();
        savedState.badgeSavedStates =
                BadgeUtils.createParcelableBadgeStates(menuView.getBadgeDrawables());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof BottomNavigationPresenter.SavedState) {
            menuView.tryRestoreSelectedItemId(((BottomNavigationPresenter.SavedState) state).selectedItemId);
            SparseArray<BadgeDrawable> badgeDrawables =
                    BadgeUtils.createBadgeDrawablesFromSavedStates(
                            menuView.getContext(), ((BottomNavigationPresenter.SavedState) state).badgeSavedStates);
            menuView.setBadgeDrawables(badgeDrawables);
        }
    }

    public void setUpdateSuspended(boolean updateSuspended) {
        this.updateSuspended = updateSuspended;
    }

    static class SavedState implements Parcelable {
        int selectedItemId;
        @Nullable
        ParcelableSparseArray badgeSavedStates;

        SavedState() {}

        SavedState(@NonNull Parcel in) {
            selectedItemId = in.readInt();
            badgeSavedStates = in.readParcelable(getClass().getClassLoader());
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(@NonNull Parcel out, int flags) {
            out.writeInt(selectedItemId);
            out.writeParcelable(badgeSavedStates, /* parcelableFlags= */ 0);
        }

        public static final Creator<SavedState> CREATOR =
                new Creator<SavedState>() {
                    @NonNull
                    @Override
                    public BottomNavigationPresenter.SavedState createFromParcel(@NonNull Parcel in) {
                        return new BottomNavigationPresenter.SavedState(in);
                    }

                    @NonNull
                    @Override
                    public BottomNavigationPresenter.SavedState[] newArray(int size) {
                        return new BottomNavigationPresenter.SavedState[size];
                    }
                };
    }
}
