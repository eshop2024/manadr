package com.example.manadr.utils.extension

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

fun Int.toPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun String.toInt(): Int {
    return Integer.parseInt(this)
}

fun Context.createImageFile(): File? {
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "img_" + timeStamp + "_"
    val storageDir = File(this.getExternalFilesDir(null).toString() + File.separator + "F99")
    if (!storageDir.exists()) storageDir.mkdirs()
    return File.createTempFile(imageFileName, ".jpg", storageDir)
}

fun Context.rotateImage(uri: Uri): Uri? {
    var ei: ExifInterface? = null
    var rotationAngle = 0
    try {
        ei = ExifInterface(uri.toString())
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotationAngle = 90
            ExifInterface.ORIENTATION_ROTATE_180 -> rotationAngle = 180
            ExifInterface.ORIENTATION_ROTATE_270 -> rotationAngle = 270
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return rotate(uri, rotationAngle.toFloat(), this)
}

fun createTimeStampFile(extension: String): String {
    val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
    return "IMG_${sdf.format(Date())}.$extension"
}

private fun rotate(photoURI: Uri, angle: Float, context: Context): Uri? {
    val source: Bitmap
    try {
        source = if (Build.VERSION.SDK_INT < 28) {
            MediaStore.Images.Media.getBitmap(context.contentResolver, photoURI)
        } else {
            val photo = ImageDecoder.createSource(context.contentResolver, photoURI)
            ImageDecoder.decodeBitmap(photo)
        }
        val matrix = Matrix()
        matrix.postRotate(angle)
        val des = Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
        val bytes = ByteArrayOutputStream()
        des.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, des,
            "ThisIsImageTitleString" + " - " + (Calendar.getInstance().getTime()),
            null
        )
        return Uri.parse(path)
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return null
}


fun Int.numberToCoupon(): String {
    return if (this > 1) {
        "coupons"
    } else {
        "coupon"
    }
}
