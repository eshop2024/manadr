package com.example.manadr.utils

interface ApiConstants {
    companion object {
        const val BASE_URL: String = "http://appointment.dev.manadrdev.com/"
        const val SECURITY_KEY: String = "Ikf5qGHzDF0rlz-Cu-7pYOHlRCLN8L9q"
        private const val API_VERSION = "api/v1.3"
        const val REQUEST_PHONE_VERIFICATION = "$API_VERSION/request-phone-verification"
        const val PHONE_VERIFICATION = "$API_VERSION/verify-phone"
        const val REGISTER_PROFILE = "$API_VERSION/me/profile"
        const val GET_PROFILE = "$API_VERSION/me/profile"
    }
}
