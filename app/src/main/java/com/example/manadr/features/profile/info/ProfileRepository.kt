package com.example.manadr.features.profile.info

import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.flow.Flow
import com.library.core.model.network.ResponseResult

interface ProfileRepository {

    fun getProfile(): Flow<ResponseResult<ProfileResponse>>
}
