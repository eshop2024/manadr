package com.example.manadr.features.on_boarding

import androidx.lifecycle.MutableLiveData
import com.example.manadr.di.model.entity.Boarding
import com.library.core.view.BaseViewModel
import javax.inject.Inject

class OnBoardingVieModel @Inject constructor(): BaseViewModel() {
    var boarding = MutableLiveData<Boarding>()
}