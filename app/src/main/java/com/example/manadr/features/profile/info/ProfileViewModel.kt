package com.example.manadr.features.profile.info

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.library.core.model.network.ResponseResult
import com.library.core.view.BaseViewModel
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val profileRepository: ProfileRepository) :
    BaseViewModel() {

    val profile = MutableLiveData<ProfileResponse>()

    init {
        viewModelScope.launch {
            profileRepository.getProfile().collect {
                when (it) {
                    is ResponseResult.Loading -> {
                        loading.value = true
                    }
                    is ResponseResult.Error -> {
                        loading.value = false
                    }
                    is ResponseResult.Success -> {
                        loading.value = false
                        profile.value = it.data
                    }
                }
            }
        }
    }
}