package com.example.manadr.features.on_boarding.detail

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.FragmentOnBoardingBinding
import com.example.manadr.di.model.entity.Boarding
import com.example.manadr.features.on_boarding.OnBoardingVieModel
import com.library.core.view.BaseFragment

class OnboardingFragment : BaseFragment<FragmentOnBoardingBinding, OnBoardingVieModel>() {

    private val boarding by lazy { arguments!!.getParcelable<Boarding>(ARG_PARAM1) }

    override fun bindingVariable(): Int = BR.viewModel

    override fun layoutId(): Int = R.layout.fragment_on_boarding

    override fun viewModel(): OnBoardingVieModel = ViewModelProvider(this, factory).get(
        OnBoardingVieModel::class.java
    )

    override fun initComponent() {
        viewModel.boarding.value = boarding
    }

    override fun subscribeData() {
    }

    companion object {
        private const val ARG_PARAM1 = "param1"
        fun newInstance(boarding: Boarding): OnboardingFragment {
            val fragment = OnboardingFragment()
            val args = Bundle()
            args.putParcelable(ARG_PARAM1, boarding)
            fragment.arguments = args
            return fragment
        }
    }
}