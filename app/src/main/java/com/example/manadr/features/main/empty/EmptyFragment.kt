package com.example.manadr.features.main.empty

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.FragmentEmptyBinding
import com.example.manadr.features.main.MainViewModel
import com.library.core.view.BaseFragment

class EmptyFragment : BaseFragment<FragmentEmptyBinding, MainViewModel>() {

    private val mPosition by lazy { arguments!!.getInt(ARG_PARAM1) }

    override fun bindingVariable(): Int = BR.viewModel

    override fun layoutId(): Int = R.layout.fragment_empty

    override fun viewModel(): MainViewModel = ViewModelProvider(this, factory).get(
        MainViewModel::class.java
    )

    override fun initComponent() {
        when (mPosition) {
            0 -> {
                binding.txtEmpty.text = "MESSAGE SCREEN"
            }
            1 -> {
                binding.txtEmpty.text = "STORE SCREEN"
            }
            2 -> {
                binding.txtEmpty.text = "HOME SCREEN"
            }
            3 -> {
                binding.txtEmpty.text = "NOTIFICATION SCREEN"
            }
            4 -> {
                binding.txtEmpty.text = "ACCOUNT SCREEN"
            }
        }
    }

    override fun subscribeData() {
    }

    companion object {
        private const val ARG_PARAM1 = "param1"
        fun newInstance(position: Int): EmptyFragment {
            val fragment = EmptyFragment()
            val args = Bundle()
            args.putInt(ARG_PARAM1, position)
            fragment.arguments = args
            return fragment
        }
    }
}