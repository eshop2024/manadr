package com.example.manadr.features.profile.info

import com.example.manadr.models.network.ApiService
import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import com.library.core.model.network.NetworkBoundRepository
import com.library.core.model.network.ResponseResult
import javax.inject.Inject

class ProfileRepositoryImp @Inject constructor(
    private val apiService: ApiService
) : ProfileRepository {

    override fun getProfile(): Flow<ResponseResult<ProfileResponse>> {
        return object : NetworkBoundRepository<ProfileResponse>() {
            override suspend fun fetchFromRemote(): Response<ProfileResponse> =
                apiService.getProfile()
        }.asFlow().buffer(1).flowOn(Dispatchers.IO)
    }
}