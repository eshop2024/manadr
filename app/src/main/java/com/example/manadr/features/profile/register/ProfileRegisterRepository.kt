package com.example.manadr.features.profile.register

import com.example.manadr.models.network.entity.ProfileRequest
import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.flow.Flow
import com.library.core.model.network.ResponseResult

interface ProfileRegisterRepository {
    fun registerProfile(profile : ProfileRequest): Flow<ResponseResult<ProfileResponse>>
}