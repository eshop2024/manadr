package com.example.manadr.features.authen.select_country

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.manadr.R

class SelectCountryCodeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_country_code)
    }
}