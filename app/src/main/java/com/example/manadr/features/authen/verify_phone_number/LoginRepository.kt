package com.example.manadr.features.authen.verify_phone_number

import com.library.core.model.network.response.AccountResponse
import com.manadr.country_picker.model.CountryCode
import kotlinx.coroutines.flow.Flow
import com.library.core.model.network.ResponseResult
import com.library.core.model.network.response.VerifyResponse

interface LoginRepository {

    fun requestPhoneVerification(
        countryCode: String,
        phoneNumber: String,
        type: String
    ): Flow<ResponseResult<VerifyResponse>>

    fun sendPhoneVerification(
        countryCode: String,
        phoneNumber: String,
        otp: String
    ): Flow<ResponseResult<AccountResponse>>

    fun saveCountry(country: CountryCode?)

    fun getCountry(): CountryCode?

    fun getFrequentlyCountry(): ArrayList<CountryCode>
}
