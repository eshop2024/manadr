package com.example.manadr.features.profile.register

import com.example.manadr.models.network.ApiService
import com.example.manadr.models.network.entity.ProfileRequest
import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import com.library.core.model.local.preference.PreferenceHelper
import com.library.core.model.network.NetworkBoundRepository
import com.library.core.model.network.ResponseResult
import javax.inject.Inject

class ProfileRegisterRepositoryImp @Inject constructor(
    private val apiService: ApiService,
    private val preferenceHelper: PreferenceHelper
) : ProfileRegisterRepository {
    override fun registerProfile(profile: ProfileRequest): Flow<ResponseResult<ProfileResponse>> {
        return object : NetworkBoundRepository<ProfileResponse>() {
            override suspend fun fetchFromRemote(): Response<ProfileResponse> =
                apiService.registerProfile(profile)
        }.asFlow().buffer(1).flowOn(Dispatchers.IO)
    }
}