package com.example.manadr.features.authen.otp_screen

import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.ActivityOtpVerificationBinding
import com.example.manadr.features.main.MainActivity
import com.example.manadr.features.profile.register.RegisterProfileActivity
import com.example.manadr.utils.Constants
import com.example.manadr.utils.extension.*
import com.library.core.view.BaseActivity

class OTPVerificationActivity : BaseActivity<ActivityOtpVerificationBinding, OTPVerifyViewModel>(),
    TextWatcher {

    private val countryCode by lazy { intent.getStringExtra(Constants.COUNTRY_CODE_BUNDLE) }

    private val phoneNumber by lazy { intent.getStringExtra(Constants.PHONE_BUNDLE) }

    private val verificationType by lazy { intent.getStringExtra(Constants.VERIFICATION_TYPE_BUNDLE) }

    override val mViewModel: OTPVerifyViewModel by viewModels { factory }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_otp_verification

    override fun initComponent() {
        binding.edtNumber1.addTextChangedListener(this)
        binding.edtNumber2.addTextChangedListener(this)
        binding.edtNumber3.addTextChangedListener(this)
        binding.edtNumber4.addTextChangedListener(this)
        binding.edtNumber5.addTextChangedListener(this)
        binding.edtNumber6.addTextChangedListener(this)

        binding.txtPhone.text = getString(R.string.str_send_otp_to, "$countryCode $phoneNumber")

        binding.btnBack.setOnSafeClickListener {
            finish()
        }

        binding.btnResend.setOnSafeClickListener {
            mViewModel.sendOTPVerification(
                countryCode.toString(),
                phoneNumber.toString(),
                verificationType.toString()
            )
        }

        binding.btnVerify.setOnSafeClickListener {
            mViewModel.doVerification(countryCode.toString(), phoneNumber.toString())
        }
        mViewModel.startCountdown()
    }

    override fun subscribeData() {
        mViewModel.timeRemain.observe(this, Observer {
            if (it > 0) {
                binding.txtRemaining.visible()
                binding.txtResend.gone()
                binding.btnResend.gone()
                binding.txtRemaining.text = getString(R.string.str_otp_remain, it)
            } else {
                binding.txtRemaining.invisible()
                binding.txtResend.visible()
                binding.btnResend.visible()
            }
        })

        mViewModel.verifyResponse.observe(this, Observer {
            mViewModel.startCountdown()
        })

        mViewModel.messsage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        mViewModel.accountResponse.observe(this, Observer {
            if (!it.is_registered)
                openActivity<RegisterProfileActivity>()
            else
                openActivity<MainActivity>()
        })
    }

    override fun afterTextChanged(editable: Editable) {
        if (editable.length == 1) {
            if (binding.edtNumber1.length() == 1) {
                binding.edtNumber2.requestFocus()
            }
            if (binding.edtNumber2.length() == 1) {
                binding.edtNumber3.requestFocus()
            }
            if (binding.edtNumber3.length() == 1) {
                binding.edtNumber4.requestFocus()
            }
            if (binding.edtNumber4.length() == 1) {
                binding.edtNumber5.requestFocus()
            }
            if (binding.edtNumber5.length() == 1) {
                binding.edtNumber6.requestFocus()
            }
            if (binding.edtNumber6.length() == 1) {
                hideKeyboard()
            }
        } else if (editable.isEmpty()) {
            if (binding.edtNumber6.length() == 1) {
                binding.edtNumber5.requestFocus()
            }
            if (binding.edtNumber5.length() == 1) {
                binding.edtNumber4.requestFocus()
            }
            if (binding.edtNumber4.length() == 0) {
                binding.edtNumber3.requestFocus()
            }
            if (binding.edtNumber3.length() == 0) {
                binding.edtNumber2.requestFocus()
            }
            if (binding.edtNumber2.length() == 0) {
                binding.edtNumber1.requestFocus()
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}


    companion object {
        const val OPT_TIME_COUNT = 60000L
    }
}