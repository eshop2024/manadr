package com.example.manadr.features.profile.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.manadr.models.network.entity.ProfileRequest
import com.library.core.model.network.response.profile.ProfileResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.library.core.model.network.ResponseResult
import com.example.manadr.utils.extension.isEmail
import com.library.core.view.BaseViewModel
import javax.inject.Inject

class ProfileRegisterViewModel @Inject constructor(private val profileRegisterRepository: ProfileRegisterRepository) :
    BaseViewModel() {
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val nric = MutableLiveData<String>()
    val email = MutableLiveData<String>()


    val profile = MutableLiveData<ProfileResponse>()

    fun doRegisterProfile() {
        if (firstName.value.isNullOrEmpty()) {
            message.value = FIRST_NAME_EMPTY
            return
        }

        if (lastName.value.isNullOrEmpty()) {
            message.value = LAST_NAME_EMPTY
            return
        }

        if (nric.value.isNullOrEmpty()) {
            message.value = NRIC_EMPTY
            return
        }

        if (email.value != null && email.value.toString().isNotEmpty()) {
            if (!email.value.toString().isEmail()) {
                message.value = INVALID_EMAIL
                return
            }
        }

        val _firstName = firstName.value.toString()
        val _lastName = lastName.value.toString()
        val _nric = nric.value.toString()
        val _emai = email.value.toString()

        val profileRequest = ProfileRequest(_emai,_firstName,_nric,_lastName)
        viewModelScope.launch {
            profileRegisterRepository.registerProfile(profileRequest).collect {
                when (it){
                    is ResponseResult.Loading ->{
                        loading.value = true
                    }
                    is ResponseResult.Error ->{
                        loading.value = false
                    }
                    is ResponseResult.Success ->{
                        loading.value = false
                        profile.value = it.data
                    }
                }
            }
        }

    }

    companion object {
        const val FIRST_NAME_EMPTY = 1
        const val LAST_NAME_EMPTY = 2
        const val NRIC_EMPTY = 3
        const val INVALID_EMAIL = 4
    }
}