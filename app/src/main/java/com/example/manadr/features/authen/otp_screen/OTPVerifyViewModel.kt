package com.example.manadr.features.authen.otp_screen

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.manadr.features.authen.verify_phone_number.LoginRepository
import com.library.core.model.network.response.AccountResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.library.core.model.network.ResponseResult
import com.library.core.model.network.response.VerifyResponse
import com.library.core.view.BaseViewModel
import javax.inject.Inject

class OTPVerifyViewModel @Inject constructor(private val loginRepository: LoginRepository) :
    BaseViewModel() {

    private var otpCountdown: CountDownTimer? = null
    val timeRemain = MutableLiveData<Long>()
    val verifyResponse = MutableLiveData<VerifyResponse>()
    val messsage = MutableLiveData<String>()
    val accountResponse = MutableLiveData<AccountResponse>()

    val otp1 = MutableLiveData<String>()
    val otp2 = MutableLiveData<String>()
    val otp3 = MutableLiveData<String>()
    val otp4 = MutableLiveData<String>()
    val otp5 = MutableLiveData<String>()
    val otp6 = MutableLiveData<String>()

    fun startCountdown() {
        stopCountDown()
        otpCountdown = object : CountDownTimer(OTPVerificationActivity.OPT_TIME_COUNT, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeRemain.value = millisUntilFinished / 1000
            }

            override fun onFinish() {
                timeRemain.value = 0
            }
        }.start()
    }

    fun stopCountDown() {
        otpCountdown?.cancel()
    }

    fun doVerification(countryCode: String, phoneNumber: String) {
        val otp = otp1.value + otp2.value + otp3.value + otp4.value + otp5.value + otp6.value
        viewModelScope.launch {
            loginRepository.sendPhoneVerification(countryCode, phoneNumber, otp).collect {
                when (it) {
                    is ResponseResult.Loading -> {
                        loading.value = true
                    }
                    is ResponseResult.Error -> {
                        loading.value = false
                        messsage.value = it.exception
                    }
                    is ResponseResult.Success -> {
                        loading.value = false
                        accountResponse.value = it.data
                    }
                }

            }
        }
    }

    fun sendOTPVerification(countryCode: String, phoneNumber: String, type: String) {
        viewModelScope.launch {
            loginRepository.requestPhoneVerification(countryCode, phoneNumber, type).collect {
                when (it) {
                    is ResponseResult.Loading -> {
                        loading.value = true
                    }
                    is ResponseResult.Error -> {
                        loading.value = false
                    }
                    is ResponseResult.Success -> {
                        loading.value = false
                        verifyResponse.value = it.data
                    }
                }

            }
        }
    }
}