package com.example.manadr.features.on_boarding

import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.example.manadr.R
import com.example.manadr.databinding.ActivityOnBoardingBinding
import com.example.manadr.features.authen.verify_phone_number.VerifyPhoneNumberActivity
import com.example.manadr.utils.extension.openActivity
import com.example.manadr.utils.extension.setOnSafeClickListener
import com.library.core.view.BaseActivity

class OnBoardingActivity : BaseActivity<ActivityOnBoardingBinding, OnBoardingVieModel>() {
    override fun getBindingVariable(): Int = 0

    override fun getLayoutId() = R.layout.activity_on_boarding

    override val mViewModel: OnBoardingVieModel by viewModels { factory }

    override fun initComponent() {
        binding.viewPager.adapter = OnboardingViewPagerAdapter(supportFragmentManager, this)
        binding.dotsIndicator.setViewPager(binding.viewPager)

        binding.txtSkip.setOnClickListener {
            openActivity<VerifyPhoneNumberActivity>()
        }
        binding.btnNext.setOnSafeClickListener {
            if (getItem(+1) > binding.viewPager.childCount - 1) {
                openActivity<VerifyPhoneNumberActivity>()
            } else {
                binding.viewPager.setCurrentItem(getItem(+1), true)
            }
        }

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (binding.viewPager.currentItem == 2) {
                    binding.btnNext.apply {
                        setTextColor(ContextCompat.getColor(this@OnBoardingActivity, R.color.white))
                        setBackgroundResource(R.drawable.bg_rounder_15dp)
                        text = getString(R.string.str_get_started)
                    }
                } else {
                    binding.btnNext.apply {
                        setTextColor(
                            ContextCompat.getColor(
                                this@OnBoardingActivity,
                                R.color.colorPrimary
                            )
                        )
                        setBackgroundResource(R.drawable.bg_rounder_border_15dp)
                        text = getString(R.string.str_next)
                    }
                }
            }

        })

    }

    override fun subscribeData() {
    }

    private fun getItem(i: Int): Int {
        return binding.viewPager.currentItem + i
    }

}