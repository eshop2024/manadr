package com.example.manadr.features.on_boarding

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.manadr.R
import com.example.manadr.di.model.entity.Boarding
import com.example.manadr.features.on_boarding.detail.OnboardingFragment

class OnboardingViewPagerAdapter(
    manager: FragmentManager,
    private val context: Context
) :
    FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    // Returns total number of pages
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                val boarding = Boarding(
                    context.resources.getString(R.string.str_title_onboarding_1),
                    context.resources.getString(R.string.str_description_onboarding_1),
                    R.drawable.ic_on_boarding_1
                )
                OnboardingFragment.newInstance(boarding)
            }
            1 -> {
                val boarding = Boarding(
                    context.resources.getString(R.string.str_title_onboarding_2),
                    context.resources.getString(R.string.str_description_onboarding_2),
                    R.drawable.ic_on_boarding_2
                )
                OnboardingFragment.newInstance(boarding)
            }
            2 ->{
                val boarding = Boarding(
                    context.resources.getString(R.string.str_title_onboarding_3),
                    context.resources.getString(R.string.str_description_onboarding_3),
                    R.drawable.ic_on_boarding_3
                )
                OnboardingFragment.newInstance(boarding)
            }
            else -> null
        }!!
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }

    companion object {
        private const val NUM_ITEMS = 3
    }

}