package com.example.manadr.features.authen.verify_phone_number

import com.example.manadr.models.network.ApiService
import com.example.manadr.models.local.AppPreferenceHelper
import com.library.core.model.network.response.AccountResponse
import com.manadr.country_picker.model.CountryCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import com.library.core.model.local.preference.PreferenceHelper
import com.library.core.model.network.NetworkBoundRepository
import com.library.core.model.network.ResponseResult
import com.library.core.model.network.response.VerifyResponse
import javax.inject.Inject

class LoginRepositoryImp @Inject
constructor(
    private val apiService: ApiService,
    private val preferenceHelper: PreferenceHelper,
    private val appPreferenceHelper: AppPreferenceHelper
) : LoginRepository {
    override fun requestPhoneVerification(
        countryCode: String,
        phoneNumber: String,
        type: String
    ): Flow<ResponseResult<VerifyResponse>> {
        return object : NetworkBoundRepository<VerifyResponse>() {
            override suspend fun fetchFromRemote(): Response<VerifyResponse> =
                apiService.requestPhoneVerification(countryCode, phoneNumber, type)
        }.asFlow().buffer(1).flowOn(Dispatchers.IO)
    }

    override fun sendPhoneVerification(
        countryCode: String,
        phoneNumber: String,
        otp: String
    ): Flow<ResponseResult<AccountResponse>> {
        return object : NetworkBoundRepository<AccountResponse>() {
            override suspend fun fetchFromRemote(): Response<AccountResponse> =
                apiService.phoneVerification(countryCode, phoneNumber, otp)

            override suspend fun handleRemoteData(response: AccountResponse) {
                preferenceHelper.token = response.token
            }
        }.asFlow().buffer(1).flowOn(Dispatchers.IO)
    }

    override fun saveCountry(country: CountryCode?) {
        appPreferenceHelper.country = country

        val frequently = appPreferenceHelper.frequentlyCountry
        if (frequently.size > 6){
            frequently.removeAt(frequently.size - 1)
        }
        frequently.add(0, country!!)
        appPreferenceHelper.frequentlyCountry = frequently

    }

    override fun getCountry() = appPreferenceHelper.country

    override fun getFrequentlyCountry() = appPreferenceHelper.frequentlyCountry

}