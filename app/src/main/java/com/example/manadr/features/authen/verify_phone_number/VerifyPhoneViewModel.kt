package com.example.manadr.features.authen.verify_phone_number

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.manadr.country_picker.model.CountryCode
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.library.core.model.network.ResponseResult
import com.library.core.model.network.response.VerifyResponse
import com.example.manadr.utils.extension.isPhoneNumber
import com.library.core.view.BaseViewModel
import javax.inject.Inject

class VerifyPhoneViewModel @Inject constructor(private val loginRepository: LoginRepository): BaseViewModel(){


    val phoneNumber = MutableLiveData<String>()
    val countryCode = MutableLiveData<String>().apply {
        value = "+84"
    }
    val verifyResponse = MutableLiveData<VerifyResponse>()

    fun doLogin() {
        val number = phoneNumber.value
        if (number == null)
            message.value =
                EMPTY_PHONE_NUMBER
        else if (!number.isPhoneNumber())
            message.value =
                INVALID_PHONE_NUMBER
        else{
            message.value =
                CHOOSE_VERIFY_TYPE
        }
    }

    fun doLogin(type: String) {
        val textPhoneNumber = phoneNumber.value.toString()
        val textCountryCode = countryCode.value.toString()
        viewModelScope.launch {
            loginRepository.requestPhoneVerification(textCountryCode, textPhoneNumber, type).collect {
                when (it){
                    is ResponseResult.Loading ->{
                        loading.value = true
                    }
                    is ResponseResult.Error ->{
                        loading.value = false
                        message.value = INVALID_PHONE_NUMBER
                    }
                    is ResponseResult.Success ->{
                        loading.value = false
                        verifyResponse.value = it.data
                    }
                }

            }
        }
    }

    fun saveCountry(country: CountryCode?) {
        loginRepository.saveCountry(country)
    }

    fun getCountry() = loginRepository.getCountry()

    fun getFrequentlyCountry() = loginRepository.getFrequentlyCountry()

    companion object{
        const val EMPTY_PHONE_NUMBER = 0
        const val INVALID_PHONE_NUMBER = 1
        const val CHOOSE_VERIFY_TYPE = 2
    }

}