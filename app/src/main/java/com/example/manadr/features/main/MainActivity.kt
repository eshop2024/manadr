package com.example.manadr.features.main

import android.annotation.SuppressLint
import android.view.Gravity
import androidx.activity.viewModels
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.ActivityMainBinding
import com.example.manadr.features.main.account.AccountFragment
import com.example.manadr.features.main.empty.EmptyFragment
import com.example.manadr.features.profile.info.ProfileViewModel
import com.example.manadr.utils.adapter.ViewPagerAdapter
import com.example.manadr.utils.custom_view.bottom_navigation.BottomNavigationItemView.LABEL_VISIBILITY_MANADR
import com.example.manadr.utils.custom_view.bottom_navigation.BottomNavigationView
import com.library.core.view.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding, ProfileViewModel>() {

    override val mViewModel: ProfileViewModel by viewModels { factory }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_main


    override fun initComponent() {
        setupViewPager()
        initListener()
    }

    private fun setupViewPager() {
        var viewPagerAdapter = ViewPagerAdapter(
            supportFragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        viewPagerAdapter.addFrag(EmptyFragment.newInstance(0), "")
        viewPagerAdapter.addFrag(EmptyFragment.newInstance(1), "")
        viewPagerAdapter.addFrag(EmptyFragment.newInstance(2), "")
        viewPagerAdapter.addFrag(EmptyFragment.newInstance(3), "")
        viewPagerAdapter.addFrag(AccountFragment.newInstance(), "")

        binding.viewPager.adapter = viewPagerAdapter
        binding.viewPager.offscreenPageLimit = viewPagerAdapter.listFrag.size
    }


    @SuppressLint("WrongConstant")
    private fun initListener() {
        val onNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.action_message -> {
                        binding.viewPager.currentItem = 0
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.action_store -> {
                        binding.viewPager.currentItem = 1
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.action_home -> {
                        binding.viewPager.currentItem = 2
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.action_notificaiton -> {
                        binding.viewPager.currentItem = 3
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.action_account -> {
                        binding.viewPager.currentItem = 4
                        return@OnNavigationItemSelectedListener true
                    }
                }
                false
            }
        binding.bottomNavMenu.labelVisibilityMode = LABEL_VISIBILITY_MANADR
        binding.bottomNavMenu.getOrCreateBadge(R.id.action_message).number = 99
        binding.bottomNavMenu.getOrCreateBadge(R.id.action_notificaiton).number = 99
        binding.bottomNavMenu.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun subscribeData() {

    }
}