package com.example.manadr.features.profile.register

import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.ActivityRegisterProfileBinding
import com.example.manadr.features.main.MainActivity
import com.example.manadr.features.profile.info.ProfileActivity
import com.example.manadr.utils.extension.openActivity
import com.example.manadr.utils.extension.setOnSafeClickListener
import com.library.core.view.BaseActivity

class RegisterProfileActivity : BaseActivity<ActivityRegisterProfileBinding, ProfileRegisterViewModel>() {

    override val mViewModel: ProfileRegisterViewModel by viewModels { factory }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_register_profile

    override fun initComponent() {
        binding.btnRegister.setOnSafeClickListener {
            resetError()
            mViewModel.doRegisterProfile()
        }
    }

    override fun subscribeData() {
        mViewModel.message.observe(this, Observer {
            when (it){
                ProfileRegisterViewModel.FIRST_NAME_EMPTY ->{
                    binding.textInputLayout.error = getString(R.string.str_first_name_required)
                    binding.editText.requestFocus()
                }

                ProfileRegisterViewModel.LAST_NAME_EMPTY ->{
                    binding.textInputLastName.error = getString(R.string.str_last_name_required)
                    binding.editLastName.requestFocus()
                }

                ProfileRegisterViewModel.NRIC_EMPTY ->{
                    binding.textInputNric.error = getString(R.string.str_nric_name_required)
                    binding.editNric.requestFocus()
                }

                ProfileRegisterViewModel.INVALID_EMAIL ->{
                    binding.textInputEmail.error = getString(R.string.str_email_is_invalid)
                    binding.editEmail.requestFocus()
                }
            }
        })

        mViewModel.profile.observe(this, Observer {
            openActivity<MainActivity>()
        })
    }

    private fun resetError(){
        binding.textInputLayout.error = null
        binding.textInputLastName.error = null
        binding.textInputNric.error = null
        binding.textInputEmail.error = null
    }
}