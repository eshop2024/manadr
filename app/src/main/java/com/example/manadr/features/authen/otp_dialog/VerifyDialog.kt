package com.example.manadr.features.authen.otp_dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.example.manadr.R
import com.example.manadr.databinding.DialogVerifySmsBinding
import com.example.manadr.utils.extension.setOnSafeClickListener

class VerifyDialog(context: Context, private val onConfirm: ((selected: String) -> Unit?)? = null) :
    Dialog(context, R.style.MyAlertDialogTheme) {

    private lateinit var binding: DialogVerifySmsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_verify_sms,
            null,
            false
        )

        setContentView(binding.root)

        initComponents()

    }

    private fun initComponents() {
        binding.btnTextMessage.setOnSafeClickListener {
            onConfirm?.invoke("sms")
            dismiss()
        }

        binding.btnVoiceCall.setOnSafeClickListener {
            onConfirm?.invoke("voice")
            dismiss()
        }
    }

}