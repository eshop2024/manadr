package com.example.manadr.features.authen.verify_phone_number

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.ActivityLoginBinding
import com.example.manadr.features.authen.otp_dialog.VerifyDialog
import com.example.manadr.features.authen.otp_screen.OTPVerificationActivity
import com.manadr.country_picker.ui.CountryCodeChooserActivity
import com.manadr.country_picker.utils.CountryCodeIntent
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import com.example.manadr.utils.Constants
import com.example.manadr.utils.extension.*
import com.library.core.view.BaseActivity

class VerifyPhoneNumberActivity : BaseActivity<ActivityLoginBinding, VerifyPhoneViewModel>() {
    override val mViewModel: VerifyPhoneViewModel by viewModels { factory }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun initComponent() {
        binding.btnSendOTP.setOnSafeClickListener {
            txtError.gone()
            mViewModel.doLogin()
        }

        val country = mViewModel.getCountry()
        binding.txtSelectCode.text = country?.phoneCode ?: "+65"

        binding.txtSelectCode.setOnSafeClickListener {
            CountryCodeChooserActivity.start(this, mViewModel.getCountry(),mViewModel.getFrequentlyCountry(), Constants.REQUEST_COUNTRY_CODE)
            //openActivity<ProfileActivity>()
        }

        binding.imgBack.setOnSafeClickListener { finish() }
    }

    override fun subscribeData() {
        mViewModel.message.observe(this, Observer {
            when (it) {
                VerifyPhoneViewModel.EMPTY_PHONE_NUMBER -> {
                    binding.txtError.text = getString(R.string.str_please_enter_your_phone_number)
                    binding.txtError.visible()
                }
                VerifyPhoneViewModel.INVALID_PHONE_NUMBER -> {
                    binding.txtError.text = getString(R.string.str_invalid_phone_number)
                    binding.txtError.visible()
                }
                VerifyPhoneViewModel.CHOOSE_VERIFY_TYPE -> {
                    handleVerifyDialog()
                }
            }
        })

        mViewModel.verifyResponse.observe(this, Observer {
            openActivity<OTPVerificationActivity>{
                putString(Constants.PHONE_BUNDLE, edtPhoneNumber.text.toString().toPhoneFormat())
                putString(Constants.COUNTRY_CODE_BUNDLE,  binding.txtSelectCode.text.toString())
                putString(Constants.VERIFICATION_TYPE_BUNDLE, "sms")
            }
        })
    }


    private fun handleVerifyDialog() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            lifecycleScope.launch {
                delay(300)
                showVerifyDialog()
            }
        } else {
            showVerifyDialog()
        }

    }

    private fun showVerifyDialog() {
        VerifyDialog(this@VerifyPhoneNumberActivity) { type ->
            mViewModel.doLogin(type)
        }.apply {
            // setCanceledOnTouchOutside(false)
            //setCancelable(false)
            show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            Constants.REQUEST_COUNTRY_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val country = CountryCodeIntent[data]
                    mViewModel.saveCountry(country)
                    binding.txtSelectCode.text = country?.phoneCode
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

}
