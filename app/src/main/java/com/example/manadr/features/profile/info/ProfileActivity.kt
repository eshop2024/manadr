package com.example.manadr.features.profile.info

import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.ActivityProfileBinding
import com.example.manadr.utils.extension.setOnSafeClickListener
import com.library.core.view.BaseActivity

class ProfileActivity : BaseActivity<ActivityProfileBinding, ProfileViewModel>(){
    override val mViewModel: ProfileViewModel by viewModels { factory }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int  = R.layout.activity_profile

    override fun initComponent() {
        binding.imgBack.setOnSafeClickListener {
            finish()
        }
        Glide.with(this).load("https://manadr-test.s3.amazonaws.com/preview-crdbk4h5sffdtnn4dbjseoc5bc-20200824091351/1598260430.jpeg")
            .apply(RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL))
            .into(binding.imgAvatar)
    }

    override fun subscribeData() {

    }
}