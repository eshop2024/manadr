package com.example.manadr.features.main.account

import androidx.lifecycle.ViewModelProvider
import com.example.manadr.BR
import com.example.manadr.R
import com.example.manadr.databinding.FragmentAccountBinding
import com.example.manadr.features.main.MainViewModel
import com.example.manadr.features.profile.info.ProfileActivity
import com.example.manadr.utils.extension.openActivity
import com.example.manadr.utils.extension.setOnSafeClickListener
import com.library.core.view.BaseFragment

class AccountFragment : BaseFragment<FragmentAccountBinding, MainViewModel>() {

    override fun bindingVariable(): Int = BR.viewModel

    override fun layoutId(): Int = R.layout.fragment_account

    override fun viewModel(): MainViewModel = ViewModelProvider(this, factory).get(
        MainViewModel::class.java
    )

    override fun initComponent() {
        binding.lyProfile.setOnSafeClickListener {
            requireActivity().openActivity<ProfileActivity>()
        }
    }

    override fun subscribeData() {
    }

    companion object {
        fun newInstance(): AccountFragment {
            return AccountFragment()
        }
    }
}